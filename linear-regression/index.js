const _ = require('lodash');
const loadCSV = require('../load-csv');
const LinearRegression = require('./linear-regression');

// const plot = require('node-remote-plot');

let { features, labels, testFeatures, testLabels } = loadCSV(
  '../data/cars.csv',
  {
    shuffle: true,
    splitTest: 50,
    dataColumns: ['horsepower', 'weight', 'displacement'],
    labelColumns: ['mpg'],
  }
);

const regression = new LinearRegression(features, labels, {
  learningRate: 0.1,
  iterations: 3,
  batchSize: 10,
});

regression.train();

const weights = regression.weights.bufferSync();
console.log('b =', weights.get(0, 0));
_.range(1, features[0].length + 1).forEach((i) =>
  console.log(`m${i} =`, weights.get(i, 0))
);

const r2 = regression.test(testFeatures, testLabels);
console.log('R^2 =', r2);

console.log(_.slice(regression.mseHistory, 0, 30));

// plot({
//   x: regression.mseHistory,
//   xLabel: 'Iteration #',
//   yLabel: 'Mean Squared Error',
// });

regression
  .predict([
    [120, 2, 380],
    [135, 2.1, 420],
  ])
  .print();
