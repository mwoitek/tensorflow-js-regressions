const _ = require('lodash');
const tf = require('@tensorflow/tfjs-node');

class LinearRegression {
  constructor(features, labels, options) {
    this.features = this.processFeatures(features);
    this.labels = tf.tensor(labels);

    this.options = Object.assign(
      { learningRate: 0.1, iterations: 1000 },
      options
    );

    this.weights = tf.zeros([this.features.shape[1], 1]);

    this.mseHistory = [];
  }

  train() {
    const { batchSize } = this.options;
    const batchQuantity = Math.floor(this.features.shape[0] / batchSize);

    for (let i = 0; i < this.options.iterations; i++) {
      for (let j = 0; j < batchQuantity; j++) {
        const startIndex = j * batchSize;

        const featuresSlice = this.features.slice(
          [startIndex, 0],
          [batchSize, -1]
        );
        const labelsSlice = this.labels.slice([startIndex, 0], [batchSize, -1]);

        this.gradientDescent(featuresSlice, labelsSlice);
      }

      this.recordMSE();
      this.updateLearningRate();
    }
  }

  gradientDescent(features, labels) {
    const currentGuesses = features.matMul(this.weights);
    const differences = currentGuesses.sub(labels);

    const slopes = features
      .transpose()
      .matMul(differences)
      .div(features.shape[0]);

    this.weights = this.weights.sub(slopes.mul(this.options.learningRate));
  }

  test(testFeatures, testLabels) {
    testFeatures = this.processFeatures(testFeatures);
    testLabels = tf.tensor(testLabels);

    const predictions = testFeatures.matMul(this.weights);

    const ssRes = testLabels.sub(predictions).pow(2).sum().bufferSync().get();
    const ssTot = testLabels
      .sub(testLabels.mean())
      .pow(2)
      .sum()
      .bufferSync()
      .get();

    return 1 - ssRes / ssTot;
  }

  processFeatures(features) {
    features = tf.tensor(features);

    if (this.mean && this.variance) {
      features = features.sub(this.mean).div(this.variance.sqrt());
    } else {
      features = this.standardize(features);
    }

    return tf.ones([features.shape[0], 1]).concat(features, 1);
  }

  standardize(features) {
    const { mean, variance } = tf.moments(features, 0);

    this.mean = mean;
    this.variance = variance;

    return features.sub(mean).div(variance.sqrt());
  }

  recordMSE() {
    const mse = this.features
      .matMul(this.weights)
      .squaredDifference(this.labels)
      .mean()
      .bufferSync()
      .get();
    this.mseHistory.push(mse);
  }

  updateLearningRate() {
    if (this.mseHistory.length < 2) {
      return;
    }

    const lastMse = _.last(this.mseHistory);
    const secondLastMse = _.chain(this.mseHistory).initial().last().value();

    if (lastMse > secondLastMse) {
      this.options.learningRate /= 2;
    } else {
      this.options.learningRate *= 1.05;
    }
  }

  predict(observations) {
    return this.processFeatures(observations).matMul(this.weights);
  }
}

module.exports = LinearRegression;
