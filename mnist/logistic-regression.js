const _ = require('lodash');
const tf = require('@tensorflow/tfjs-node');

class LogisticRegression {
  constructor(features, labels, options) {
    this.features = this.processFeatures(features);
    this.labels = tf.tensor(labels);

    this.options = Object.assign(
      {
        learningRate: 0.1,
        iterations: 1000,
      },
      options
    );

    this.weights = tf.zeros([this.features.shape[1], this.labels.shape[1]]);

    this.costHistory = [];
  }

  train() {
    const { batchSize } = this.options;
    const batchQuantity = Math.floor(this.features.shape[0] / batchSize);

    for (let i = 0; i < this.options.iterations; i++) {
      for (let j = 0; j < batchQuantity; j++) {
        const startIndex = j * batchSize;

        const featuresSlice = this.features.slice(
          [startIndex, 0],
          [batchSize, -1]
        );
        const labelsSlice = this.labels.slice([startIndex, 0], [batchSize, -1]);

        this.gradientDescent(featuresSlice, labelsSlice);
      }

      this.recordCost();
      this.updateLearningRate();
    }
  }

  gradientDescent(features, labels) {
    const currentGuesses = features.matMul(this.weights).softmax();
    const differences = currentGuesses.sub(labels);

    const slopes = features
      .transpose()
      .matMul(differences)
      .div(features.shape[0]);

    this.weights = this.weights.sub(slopes.mul(this.options.learningRate));
  }

  test(testFeatures, testLabels) {
    const predictions = this.predict(testFeatures);
    testLabels = tf.tensor(testLabels).argMax(1);

    const numPredictions = predictions.shape[0];
    const incorrect = predictions.notEqual(testLabels).sum().bufferSync().get();

    return (numPredictions - incorrect) / numPredictions;
  }

  processFeatures(features) {
    features = tf.tensor(features);

    if (this.mean && this.variance) {
      features = features.sub(this.mean).div(this.variance.sqrt());
    } else {
      features = this.standardize(features);
    }

    return tf.ones([features.shape[0], 1]).concat(features, 1);
  }

  standardize(features) {
    const { mean, variance } = tf.moments(features, 0);

    const filler = variance.cast('bool').logicalNot().cast('float32');

    this.mean = mean;
    this.variance = variance.add(filler);

    return features.sub(mean).div(this.variance.sqrt());
  }

  recordCost() {
    const guesses = this.features.matMul(this.weights).softmax();

    const termOne = this.labels.transpose().matMul(guesses.log());
    const termTwo = this.labels
      .mul(-1)
      .add(1)
      .transpose()
      .matMul(guesses.mul(-1).add(1).log());

    const cost = termOne
      .add(termTwo)
      .div(this.features.shape[0])
      .mul(-1)
      .bufferSync()
      .get();
    this.costHistory.push(cost);
  }

  updateLearningRate() {
    if (this.costHistory.length < 2) {
      return;
    }

    const lastCost = _.last(this.costHistory);
    const secondLastCost = _.chain(this.costHistory).initial().last().value();

    if (lastCost > secondLastCost) {
      this.options.learningRate /= 2;
    } else {
      this.options.learningRate *= 1.05;
    }
  }

  predict(observations) {
    return this.processFeatures(observations)
      .matMul(this.weights)
      .softmax()
      .argMax(1);
  }
}

module.exports = LogisticRegression;
